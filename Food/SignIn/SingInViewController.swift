//
//  SingInViewController.swift
//  Food
//
//  Created by GIS on 7/25/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class SingInViewController: UIViewController {

    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
    }
    
    @IBAction func signInButton(_ sender: UIButton) {
        
        if phoneNumber.text! == "12345" , password.text! == "12345" {
            
            self.dismiss(animated: true, completion: nil)
            UserDefaults.standard.set(true, forKey: "isLogin")
            navigationController?.popViewController(animated: true)
            
        } else {
            
            showAlertMessage(title: "Sign In", message: "Your phone number or password is incorrect. Please try again")
        }
    }
    
    //Show Alert Message
    func showAlertMessage(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "NO", style: .default) { (action) in
            // Close current view controller
        }
        
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func backToFoodDetail(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
}
