//
//  ShowMapViewController.swift
//  Food
//
//  Created by GIS on 7/24/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import GooglePlaces

class ShowMapViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,GMSAutocompleteResultsViewControllerDelegate {
    

    @IBOutlet weak var map: GMSMapView!
    var locationManager = CLLocationManager()
    var currentLocation = CLLocation()
    var centerMapCoordinate: CLLocationCoordinate2D!
    var buttonLocation : UIButton!
    var imageView = UIImageView()
    var icon = UIView()
    var marker = GMSMarker()
    var locationString = String()
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var itemBar = UITabBarController()
    var itemButtonBar = UIBarButtonItem()
    var isSelectStore = true
    var isSelectFarm = true
    
    var userLatAndLong = [[11.5750552905215,104.889334514737],[11.4952487942592,104.885341376066],[11.5752868529741,104.888805113733],[11.5752747000559,104.888831600547],[11.5913341564719,104.879408665001]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.map.isMyLocationEnabled = true
        self.map.userActivity?.delegate = self as? NSUserActivityDelegate
        self.map.delegate = self
        self.map.selectedMarker = marker
        self.map.settings.myLocationButton = true
        
        
        // User Location
        self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        // Present the Autocomplete view controller when the button is pressed.
        resultsViewController = GMSAutocompleteResultsViewController ()
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        let subView = UIView(frame: CGRect(x: 0, y: 65, width: 350, height: 45))
        subView.addSubview((searchController?.searchBar)!)
        self.view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        self.definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if !isSelectStore {
           
            hideUserLocation()
            //Call Fuction
            createButtonForClickLocationStore()
            
        } else if !isSelectFarm {
            
            hideUserLocation()
            //Call Fuction
            createButtonForClickLocationFarm()
            
        } else {
            
            showUserOnMap()
            //Call Fuction
            createButtonForClickLocationTabMap()
            
        }
    }
    
    @objc func backToOrder() {
     
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        
        searchController?.isActive = false
        
        let camera = GMSCameraPosition.camera(withLatitude: (place.coordinate.latitude), longitude: (place.coordinate.longitude), zoom: 10)
        
        mapView(map, didChange: camera)
        self.map.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
        centerMapCoordinate = CLLocationCoordinate2D(latitude: place.coordinate.latitude , longitude: place.coordinate.longitude)
        //show marker
        self.marker.map = self.map
        self.marker.position = self.centerMapCoordinate
        
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
     
        print("Error \(error)")
        
    }
    
    //show lat and long when move the map
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        let lat = mapView.camera.target.latitude
        let long = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: lat , longitude: long)
        self.placeMarkerOnCenter(position: centerMapCoordinate)

        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(centerMapCoordinate) { response, error in

            let address = response?.firstResult()
            let lines = address?.lines
        }

        print(lat)
        print(long)
    }
    
    //click on marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        
        self.performSegue(withIdentifier: "orderPage", sender: nil)
        return true
    }
    
    //show the lat and long when click a place on map
    private func mapView(_ mapView: GMSMapView, didTap coordinate: CLLocationCoordinate2D) {
        
        let lat = mapView.camera.target.latitude
        let long = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: lat , longitude: long)
        self.placeMarkerOnCenter(position: centerMapCoordinate)

        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(centerMapCoordinate) { response, error in

            let address = response?.firstResult()
            let lines = address?.lines
            self.locationString = lines!.joined(separator: " , ")
            print("location",self.locationString)
            print(lines?.joined(separator: " , "))
        }

        print(lat)
        print(long)

    }
    
    //zoom to current location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 10)
        self.map.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
    }

    func placeMarkerOnCenter(position:CLLocationCoordinate2D) {
        //Create Marker when click
    }
    
    func showUserOnMap() {
        
        for user in userLatAndLong {
            
            let camera = GMSCameraPosition.camera(withLatitude: user[0] , longitude: user[1] , zoom: 15)
            let location = CLLocationCoordinate2D(latitude: user[0], longitude: user[1])
            let mark = GMSMarker()
            
            icon = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
          
            mark.iconView = icon
            mark.iconView?.layer.cornerRadius = 30
            mark.iconView?.layer.borderWidth = 1
            mark.iconView?.layer.borderColor = UIColor.red.cgColor
            
            imageView.image = UIImage(named: "image3")
            imageView.layer.cornerRadius  = 30
            imageView.clipsToBounds = true
            
            mark.position = location
            mark.map = map
            icon.addSubview(imageView)
            icon.tag = 100
        }
    }
   
    func  hideUserLocation()  {
        
        print("Start remove sibview")
        if let viewWithTag = self.icon.viewWithTag(100) {
            
            viewWithTag.removeFromSuperview()
            print("Yes")
                
        } else {
                
            print("No!")
        }
    }
    
    @objc func buttonPickUPByUser() {
        
        let point = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.latitude)
        mapView(map, didTap: point)
        
        showAlertMessageLogin(title: "SET DROP OFF", message: "Are you sure to use this location?")
    }
    
    func createButtonForClickLocationFarm()  {
        
        buttonLocation = UIButton(frame: CGRect(x : CGFloat(100), y: CGFloat(100), width: CGFloat(30), height: CGFloat(50)))
        buttonLocation.setImage(UIImage(named: "street-view-solid"), for: UIControlState.normal)
        buttonLocation.center = self.view.center
        buttonLocation.layer.zPosition = 2
        buttonLocation.addTarget(self, action: #selector(buttonPickUPByUser), for: .touchUpInside)
        
        self.view.addSubview(buttonLocation)
    }
    
    func createButtonForClickLocationStore()  {
        
        buttonLocation = UIButton(frame: CGRect(x : CGFloat(100), y: CGFloat(100), width: CGFloat(30), height: CGFloat(50)))
        buttonLocation.setImage(UIImage(named: "mapStore"), for: UIControlState.normal)
        buttonLocation.center = self.view.center
        buttonLocation.layer.zPosition = 2
        buttonLocation.addTarget(self, action: #selector(buttonPickUPByUser), for: .touchUpInside)
        
        self.view.addSubview(buttonLocation)
    }
    
    func createButtonForClickLocationTabMap()  {
        
        buttonLocation = UIButton(frame: CGRect(x : CGFloat(100), y: CGFloat(100), width: CGFloat(100), height: CGFloat(70)))
        buttonLocation.setImage(UIImage(named: "pinSetDropOff"), for: UIControlState.normal)
        buttonLocation.center = self.view.center
        buttonLocation.layer.zPosition = 2
        buttonLocation.addTarget(self, action: #selector(buttonPickUPByUser), for: .touchUpInside)
        
        self.view.addSubview(buttonLocation)
    }
    
    //show alert message
    func showAlertMessageLogin(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: .default) { (action) in
            
            orderFoodDetailViewController.getLocation = self.locationString
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
            
            //show marker
            self.marker.map = self.map
            self.marker.position = self.centerMapCoordinate
    }
        
        let cencelAction = UIAlertAction(title: "NO", style: .default) { (action) in
            
            //stay in current screen
        }
        
        alertController.addAction(cencelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
}











