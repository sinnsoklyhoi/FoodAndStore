//
//  mapDetailRestuarantTableViewCell.swift
//  Food
//
//  Created by GIS on 8/9/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class mapDetailRestuarantTableViewCell: UITableViewCell {

    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var disPrice: UILabel!
    @IBOutlet weak var quality: UILabel!
    @IBOutlet weak var subQuality: UIButton!
    @IBOutlet weak var addQuality: UIButton!
    @IBOutlet weak var viewCell: UIView!
    
}
