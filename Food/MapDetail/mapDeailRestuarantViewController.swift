//
//  mapDeailRestuarantViewController.swift
//  Food
//
//  Created by GIS on 8/9/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class mapDeailRestuarantViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tbVC: UITableView!
    
    var Food1s = ["A","B","C","D","E"]
    var images = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5")]
    var prices = ["1$","2$","3$","4$","5$","6$"]
    var disPrices = ["0.5$","1.5$","2.5$","3.5$","4.5$"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbVC.delegate = self
        tbVC.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Food1s.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "Detail", for: indexPath) as! mapDetailRestuarantTableViewCell
        cell.foodImage.image = images[indexPath.row]
        cell.foodName.text = Food1s[indexPath.row]
        cell.price.text = prices[indexPath.row]
        cell.disPrice.text = disPrices[indexPath.row]
        return cell
    }
    
}
