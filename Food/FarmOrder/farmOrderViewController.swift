//
//  farmOrderViewController.swift
//  Food
//
//  Created by GIS on 8/3/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class farmOrderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var viewOrder: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var orderLocation: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var tatalDiscount: UILabel!
    @IBOutlet weak var delivery: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var buttonOrder: UIButton!
    
    public static var getLocation = ""
    
    @IBOutlet weak var tbVC: UITableView!
    
    let images = ["image2", "image3","image4"]
    var sizes = ["Small","Middle","Big"]
    var prices = ["10$","20$","30$"]
    var qualitys = ["1"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tbVC.dataSource = self
        tbVC.delegate = self
        
        // Current Date And Time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd"
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from:  date)
        let minute = calendar.component(.minute, from:  date)
        self.dateLabel.text = dateFormatter.string(from: date)
        self.timeLabel.text = String(hour) + ":" + String(minute) + " AM"
        
        //Change Style
        self.viewDate.layer.cornerRadius = 5
        self.viewOrder.layer.cornerRadius = 5
        self.viewTotal.layer.cornerRadius = 5
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.orderLocation.text = orderFoodDetailViewController.getLocation
        if orderLocation.text?.isEmpty == true {
            
            orderLocation.text = "Please choose location first"
            orderLocation.textColor = UIColor.red
            buttonOrder.isEnabled = false
            
        } else {
            
            orderLocation.textColor = UIColor.black
            buttonOrder.isEnabled = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FarmDetail", for: indexPath) as! farmOrderItemTableViewCell
        
        cell.farmImage.image = UIImage(named: images[indexPath.row])
        cell.farmSize.text = sizes[indexPath.row]
        cell.farmPrice.text = prices[indexPath.row]
        cell.farmQuality.text = qualitys[indexPath.row]
        
        //change style
        cell.viewCell.layer.cornerRadius = 5
        
        cell.farmSize.layer.borderWidth = 1
        cell.farmSize.layer.borderColor = UIColor.red.cgColor
        cell.farmSize.layer.cornerRadius = 5
        
        cell.viewCell.layer.cornerRadius = 5
        
        return cell
        
    }
    
    //Back To Farm Detail
    @IBAction func backToFarmDetailButton(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
        
    }
    
    // Button Order
    @IBAction func orderButton(_ sender: UIButton) {
        
        var isLogin : Bool
        
        isLogin = UserDefaults.standard.bool(forKey: "isLogin")
        
        if isLogin {
            
            showAlertMessage(title: "ORDER", message: "Success!")
            
        } else {
            showAlertMessageLogin(title: "Sign In Your Account", message: "Your haven't sign in yet.")
        }
    }
    
    @IBAction func showMapButton(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "Order", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "Order" {
            
            let desination = segue.destination as! ShowMapViewController
            desination.isSelectFarm = false
        }
    }
    
    //Show Alert Message
    func showAlertMessage(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            // Go to first page

            if let vcStack = self.navigationController?.viewControllers
            {
                for vc in vcStack {
                
                        self.navigationController?.popToViewController(vc, animated: true)
                        break
                }
            }
        }
        
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertMessageLogin(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: .default) { (action) in
            // Close current view controller
            self.performSegue(withIdentifier: "login", sender: nil)
        }
        
        let cencelAction = UIAlertAction(title: "NO", style: .default) { (action) in
            // Goto Screen Login
        }
    
        alertController.addAction(cencelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
}













