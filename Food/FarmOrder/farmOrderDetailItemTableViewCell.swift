//
//  farmOrderDetailItemTableViewCell.swift
//  Food
//
//  Created by GIS on 8/3/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class farmOrderDetailItemTableViewCell: UITableViewCell {

    @IBOutlet weak var nameFood: UILabel!
    @IBOutlet weak var size: UITextField!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var totalPrices: UILabel!
    @IBOutlet weak var subButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var quality: UILabel!
    
}
