//
//  farmOrderItemTableViewCell.swift
//  Food
//
//  Created by GIS on 8/3/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class farmOrderItemTableViewCell: UITableViewCell {

    @IBOutlet weak var farmImage: UIImageView!
    @IBOutlet weak var farmSize: UILabel!
    @IBOutlet weak var farmPrice: UILabel!
    @IBOutlet weak var farmQuality: UILabel!
    @IBOutlet weak var viewCell: UIView!
    
}
