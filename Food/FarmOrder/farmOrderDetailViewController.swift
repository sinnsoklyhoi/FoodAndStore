//
//  farmOrderDetailViewController.swift
//  Food
//
//  Created by GIS on 8/3/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class farmOrderDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tbVC: UITableView!
    @IBOutlet weak var viewAddToCard: UIView!
    
    var food = ["1"]
    var items = 1
    var a = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tbVC.dataSource = self
        tbVC.delegate = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return food.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderFood", for: indexPath) as! farmOrderDetailItemTableViewCell
        cell.nameFood.text = "A"
        cell.size.text = "1L"
        cell.price.text = "1$"
        cell.totalPrices.text = "2"
        cell.quality.text = String(items)
        
        return cell
    }
    
    @IBAction func subQualityButton(_ sender: UIButton) {
        
        items -= 1
        if items == 0 {
            
            items = 1
            tbVC.reloadData()
        }
        tbVC.reloadData()
        
    }
    
    @IBAction func addQualityButton(_ sender: UIButton) {
        
        items += 1
        tbVC.reloadData()
    }
    
    @IBAction func addToCardButton(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func backToFarmDeatil(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
}
















