//
//  listStoreViewController.swift
//  Food
//
//  Created by GIS on 8/13/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class listStoreViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet var tableVC: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var buttomFileter: UIButton!
    
    var viewFileter = UIView()
    var viewSearch = UIView()
    var viewPicker = UIView()
    let picker = UIPickerView()
    var restaurantsShow = [String]()
    
    var stores = ["Store1","Store2","Store3","Store4","Store5"]
    var Locations = ["Location1","Location2","Location3","Location4","Location5",]
    var Picture = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5")]
    var citys = ["Phnom Penh","Kompong Cham","Bathdom Bong","Kompong Soum","Seama reap" , "Kom Pot", "born teaymeanchey"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableVC.delegate = self
        tableVC.dataSource = self
        
        picker.dataSource = self
        picker.delegate = self
        
        buttomFileter.tag = 1
        
        // Change style Layout
        self.buttomFileter.layer.cornerRadius = 20
        self.searchBar.layer.cornerRadius = 5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Dissmiss Keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard(dissmiss:)))
        view.addGestureRecognizer(tap)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Restaurant", for: indexPath) as! restaurantItemTableViewCell
        cell.restaurantName.text =  stores[indexPath.row]
        cell.resLocation.text = Locations[indexPath.row]
        cell.imageUrl.image = Picture[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let select = tableView.cellForRow(at: indexPath)
        self.performSegue(withIdentifier: "orderPage", sender: select)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return citys[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return citys.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @objc func dissmissKeyboard(dissmiss : UITapGestureRecognizer)  {
        
        self.view.endEditing(true)
        dissmiss.cancelsTouchesInView = false
    }
    
    @IBAction func showListCitys(_ sender: UIButton) {

        if buttomFileter.tag == 1 {
           
            buttomFileter.setTitle("Done", for: .normal)
            donePressed()
            buttomFileter.tag = 2
            print("Hello")
            
        } else {
           
            buttomFileter.setTitle("Citys", for: .normal)
            donePressedToClosePickerView()
            buttomFileter.tag = 1
        }
    }
    
    func donePressed() {
        
        //PickerView
        picker.frame = CGRect(x: 5, y: self.view.bounds.height + 20, width: self.view.safeAreaLayoutGuide.layoutFrame.width - 10, height: 250)
        view.addSubview(picker)
        picker.backgroundColor = UIColor(red: 10, green: 10 , blue: 10, alpha: 1)
        
        UIView.animate(withDuration: 0.5) {
            
            self.picker.frame.origin.y = self.view.bounds.height - 250
            
        }
        
        picker.selectRow(3, inComponent: 0, animated: true)
    }
    
    func donePressedToClosePickerView() {
            
            //PickerView
            picker.frame = CGRect(x: 5, y: self.view.bounds.height - 20, width: self.view.safeAreaLayoutGuide.layoutFrame.width - 10, height: 250)
            view.addSubview(picker)
            picker.backgroundColor = UIColor(displayP3Red: 220, green: 226, blue: 226, alpha: 1)
        
            UIView.animate(withDuration: 0.5) {
                
                self.picker.frame.origin.y = self.view.bounds.height + 250
                
            }
        
    }
    
}






















