//
//  searchRestaurantsTableViewController.swift
//  Food
//
//  Created by GIS on 7/24/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit
import GoogleMaps

class searchRestaurantsTableViewController: UITableViewController, UISearchBarDelegate, UISearchDisplayDelegate {
    
    @IBOutlet var tableVC: UITableView!
    
    var Restaurants = ["Store1","Store2","Store3","Store4","Store5"]
    var restaurantsShow = [String]()
    var Locations = ["Location1","Location2","Location3","Location4","Location5",]
    var Picture = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5")]
    var searchBar : UISearchBar!
    var viewFileter = UIView()
    var viewSearch = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Call Notification
        NotificationCenter.default.addObserver(self, selector: #selector(show(notification:)), name: Notification.Name("show") , object: nil)
    }
    
    // Function Notification
    @objc func show(notification : NSNotification)  {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Dismiss keyboard
        view.endEditing(true)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Restaurants.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Restaurant", for: indexPath) as! restaurantItemTableViewCell
        cell.restaurantName.text =  Restaurants[indexPath.row]
        cell.resLocation.text = Locations[indexPath.row]
        cell.imageUrl.image = Picture[indexPath.row]
        return cell
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let seletedCell = tableView.cellForRow(at: indexPath)
        performSegue(withIdentifier: "orderPage", sender: seletedCell)
    }
    
//    @IBAction func locationStoreBarButton(_ sender: UIBarButtonItem){
//        
//        self.performSegue(withIdentifier: "show", sender: nil)
//    }
//    

}
















