//
//  showLocationViewController.swift
//  Food
//
//  Created by GIS on 8/13/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class showLocationViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var viewCitys: UIView!
    @IBOutlet weak var tbVC: UITableView!
    @IBOutlet weak var viewVC: UIView!
    @IBOutlet weak var buttonCancel: UIButton!
    
    var citys = ["Phnom Penh","Bathdom bong","Kompong Cham","Pou Sat"]

    override func viewDidLoad() {
        super.viewDidLoad()

        tbVC.dataSource = self
        tbVC.delegate = self
        self.viewVC.layer.borderWidth = 1
        self.viewVC.layer.borderColor = UIColor.red.cgColor
        self.viewVC.layer.cornerRadius = 5
        self.viewCitys.layer.cornerRadius = 5
        self.buttonCancel.layer.cornerRadius = 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return citys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Citys", for: indexPath) as! showLocationItemTableViewCell
        
        cell.citysOrProvinces.text = citys[indexPath.row]
        
        return cell
    }

    @IBAction func cancelButton(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
