//
//  farmdetailItemTableViewCell.swift
//  Food
//
//  Created by GIS on 8/3/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class farmdetailItemTableViewCell: UITableViewCell {

    
    @IBOutlet weak var farmImage: UIImageView!
    @IBOutlet weak var farmPlace: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var disPrice: UILabel!
    @IBOutlet weak var subQuality: UIButton!
    @IBOutlet weak var quality: UILabel!
    @IBOutlet weak var addQuality: UIButton!
    @IBOutlet weak var viewCell: UIView!
    
    
}
