//
//  farmdetailViewController.swift
//  Food
//
//  Created by GIS on 8/3/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class farmdetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tbVC: UITableView!
    @IBOutlet weak var qualityItem: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var farmImage: UIImageView!
    @IBOutlet weak var viewOpen: UIView!
    @IBOutlet weak var viewClose: UIView!
    @IBOutlet weak var menu1: UIView!
    @IBOutlet weak var menu2: UIView!
    @IBOutlet weak var menu3: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var buttonCheckOut: UIButton!
    
    
    var Food1s = ["A","B","C","D","E"]
    var images = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5")]
    var prices = ["1","2","3","4","5","6"]
    var disPrices = ["0.5$","1.5$","2.5$","3.5$","4.5$"]
    var qualitys = [0,0,0,0,0]
    var total = [0,0,0,0,0]
    var storePrices = [String]()
    
    var buttonUIBarButtonItem = UIBarButtonItem()
    var labelQulity = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbVC.delegate = self
        tbVC.dataSource = self
        
        storePrices = prices
        
        //Change Style
        self.farmImage.layer.cornerRadius = 40
        self.viewImage.layer.cornerRadius = 40
        self.menu1.layer.borderColor = UIColor.red.cgColor
        self.menu1.layer.borderWidth = 1
        self.menu1.layer.cornerRadius = 5
        
        self.menu2.layer.borderColor = UIColor.red.cgColor
        self.menu2.layer.borderWidth = 1
        self.menu2.layer.cornerRadius = 5
        
        self.menu3.layer.borderColor = UIColor.red.cgColor
        self.menu3.layer.borderWidth = 1
        self.menu3.layer.cornerRadius = 5
        
        self.viewOpen.layer.borderWidth = 1
        self.viewOpen.layer.borderColor = UIColor.red.cgColor
        
        self.viewClose.layer.borderWidth = 1
        self.viewClose.layer.borderColor = UIColor.red.cgColor
        
        self.buttonCheckOut.layer.cornerRadius = 5
        
        //Call Fucntion
        createTabBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        qualitys = [0,0,0,0,0]
        storePrices = prices
        
        qualityItem.text = "0"
        totalPrice.text = "0" + "$"
        labelQulity.text = "0"
        
        tbVC.reloadData()
        
        self.buttonUIBarButtonItem.tintColor = .white
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Food1s.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "farmDetail", for: indexPath) as! farmdetailItemTableViewCell
        
        // strikethroughstyle on text
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: storePrices[indexPath.row])
        attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: UIColor.red, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        
        cell.farmImage.image = images[indexPath.row]
        cell.farmPlace.text = Food1s[indexPath.row]
        cell.disPrice.text = disPrices[indexPath.row]
        cell.quality.text = String(qualitys[indexPath.row])
        cell.addQuality.tag = indexPath.row
        cell.subQuality.tag = indexPath.row
        cell.price.text = "($\(storePrices[indexPath.row]))"
        print("Price","($\(storePrices[indexPath.row]))")
        cell.price.attributedText = attributeString 
        
        // Change Style
        cell.addQuality.layer.borderColor = UIColor.gray.cgColor
        cell.addQuality.layer.borderWidth = 1
        cell.addQuality.layer.cornerRadius = 15
        
        cell.subQuality.layer.borderColor = UIColor.gray.cgColor
        cell.subQuality.layer.borderWidth = 1
        cell.subQuality.layer.cornerRadius = 15
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let select = tableView.cellForRow(at: indexPath)
        self.performSegue(withIdentifier: "Detail", sender: select)
    }
    
    func createTabBarButton()  {
    
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 40))
        button.setImage(UIImage(named: "myCard"), for: .normal)
        button.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        labelQulity.frame = CGRect(x: button.frame.width - 25, y: 0, width: 20, height: 20)
        
        labelQulity.text = "0"
        labelQulity.textAlignment = .center
        labelQulity.backgroundColor = .white
        labelQulity.clipsToBounds = true
        labelQulity.layer.borderWidth = 1
        labelQulity.layer.borderColor = UIColor.red.cgColor
        labelQulity.layer.cornerRadius = 10
        labelQulity.textColor = .black
        
        button.addSubview(labelQulity)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    @objc func clickButton(){
        
        let convertLabelQulityToInterger = (labelQulity.text! as NSString).integerValue
        
        if convertLabelQulityToInterger == 0 {
            
        } else {
            
            performSegue(withIdentifier:"OrderDetail", sender: nil)
        }
    }
    
    @IBAction func subQualityButton(_ sender: UIButton) {
        
        if qualitys[sender.tag] == 0 {
            print("no")
            
        } else {
            qualitys[sender.tag] -= 1
            let totalQuality = qualitys.reduce(0, +)
            qualityItem.text = String(totalQuality)
            labelQulity.text = qualityItem.text
            
            let firstPrice = prices[sender.tag]
            storePrices.append(firstPrice)
            let convertPriceToString = (firstPrice as NSString).integerValue
            let totalPrices = String( convertPriceToString * qualitys[sender.tag])
            storePrices[sender.tag] = totalPrices 
            
            //Add Total Price
            total[sender.tag] = (totalPrices as NSString).integerValue
            let totals = total.reduce(0, +)
            totalPrice.text = String(totals) + "$"
            tbVC.reloadData()
        }
        
    }
    
    @IBAction func addQualityButton(_ sender: UIButton) {
        
        qualitys[sender.tag] += 1
        let firstPrice = prices[sender.tag]
        storePrices.append(firstPrice)
        
        let convertPriceToString = (firstPrice as NSString).integerValue
        let totalPrices = String( convertPriceToString * qualitys[sender.tag])
        storePrices[sender.tag] = totalPrices
        
        //Add Quality Item
        let totalQuality = qualitys.reduce(0, +)
        qualityItem.text = String(totalQuality)
        labelQulity.text = qualityItem.text
        
        //Add Total Price
        total[sender.tag] = (totalPrices as NSString).integerValue
        let totals = total.reduce(0, +)
        totalPrice.text = String(totals) + "$"
        tbVC.reloadData()
        
        
    }
    
    // Button Checkout
    @IBAction func checkOutButton(_ sender: UIButton) {
        
        let convertQualityItemToInterger = (qualityItem.text! as NSString).integerValue
        
        if convertQualityItemToInterger == 0 {
            
            showAlertMessageCheckOut(title: "ORDER", message: "Please order")
            
        } else {
            
            self.performSegue(withIdentifier: "OrderDetail", sender: nil)
        }
    }
    //Back to farm store
    @IBAction func backToFarmStore(_ sender: UIBarButtonItem) {
        
        let convertQualityItemToInterger = (qualityItem.text! as NSString).integerValue
        
        if convertQualityItemToInterger == 0  {
            
            self.dismiss(animated: true, completion: nil)
            navigationController?.popViewController(animated: true)
            
        } else {
            
            showAlertMessage(title: "ORDER", message: "Do you want to close ? if you close your food will cleared")
        }
        
        
    }
    //Show Message
    func showAlertMessageCheckOut(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            // Close current view controller
        }
        
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    //Show Alert Message
    func showAlertMessage(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "NO", style: .default) { (action) in
            // Close current view controller
        }
        
        let cencelAction = UIAlertAction(title: "YES", style: .default) { (action) in
            // Close current view controller
            self.navigationController?.popViewController(animated: true)
            
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cencelAction)
        present(alertController, animated: true, completion: nil)
        
    }
}














