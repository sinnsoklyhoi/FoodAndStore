//
//  ShowListFoddViewController.swift
//  Food
//
//  Created by GIS on 7/24/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class ShowListFoddViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var tbVC: UITableView!
    @IBOutlet weak var citysButton: UIButton!
    
    
    var Foods = ["A","B","C","D","E","F"]
    var location = ["Phnom Penh","Kompong Cham","Takaev","Seama Reab","Kror Jes","Ta Kmov"]
    var imageFoods = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5"),#imageLiteral(resourceName: "image4")]
    var citys = ["Phnom Penh","Kompong Cham","Bathdom Bong","Kompong Soum","Seama reap" , "Kom Pot", "born teaymeanchey"]
    var pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tbVC.dataSource = self
        tbVC.delegate = self
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        //Change Style
        self.citysButton.layer.cornerRadius = 20
        citysButton.tag = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Dissmiss Keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(dissmissKeyboard(dissmiss:)))
        view.addGestureRecognizer(tap)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Foods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "FarmStore", for: indexPath) as! foodItemTableViewCell
        
        cell.farmPlace.text = Foods[indexPath.row]
        cell.location.text = location[indexPath.row]
        cell.imageFood.image = imageFoods[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let seletedCell = tableView.cellForRow(at: indexPath)
        performSegue(withIdentifier: "Detail", sender: seletedCell)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return citys[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return citys.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    @IBAction func citysButton(_ sender: UIButton) {
        
        if citysButton.titleLabel?.text == "Citys" {
            
            citysButton.setTitle("Done", for: .normal)
            donePressed()
            
        } else {
            
            citysButton.setTitle("Citys", for: .normal)
            donePressedToClosePickerView()
        }
    }
    
    func donePressed() {
        
        //PickerView
        pickerView.frame = CGRect(x: 5, y: self.view.bounds.height + 20, width: self.view.safeAreaLayoutGuide.layoutFrame.width - 10, height: 250)
        view.addSubview(pickerView)
        pickerView.backgroundColor = UIColor(red: 10, green: 10 , blue: 10, alpha: 1)
        UIView.animate(withDuration: 0.5) {
            
            self.pickerView.frame.origin.y = self.view.bounds.height - 250
    
        }
        
        pickerView.selectRow(3, inComponent: 0, animated: true)
    }
    
    func donePressedToClosePickerView() {
        
        //PickerView
        pickerView.frame = CGRect(x: 5, y: self.view.bounds.height - 20, width: self.view.safeAreaLayoutGuide.layoutFrame.width - 10, height: 250)
        view.addSubview(pickerView)
        pickerView.backgroundColor = UIColor(displayP3Red: 220, green: 226, blue: 226, alpha: 1)
        
        UIView.animate(withDuration: 0.5) {
            
            self.pickerView.frame.origin.y = self.view.bounds.height + 250
            
        }
        
    }
    
    @objc func dissmissKeyboard(dissmiss : UITapGestureRecognizer)  {
        
        self.view.endEditing(true)
        dissmiss.cancelsTouchesInView = false
    }
    
    @IBAction func backToRestuarentsButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func orderButton(_ sender: Any) {
        
        performSegue(withIdentifier: "SingIn", sender: nil)
    }
    
}








