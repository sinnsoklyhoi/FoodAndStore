//
//  historyOrderTableViewCell.swift
//  Food
//
//  Created by GIS on 8/2/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class historyOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var orderLocation: UILabel!
    @IBOutlet weak var dateOrder: UILabel!
    @IBOutlet weak var timeOrder: UILabel!
    @IBOutlet weak var viewHistoryCell: UIView!
   
}
