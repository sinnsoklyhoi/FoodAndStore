//
//  historyDetailViewController.swift
//  Food
//
//  Created by GIS on 8/8/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class historyDetailViewController: UIViewController {

    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewTotal: UIView!
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var labelSize: UILabel!
    @IBOutlet weak var viewDriver: UIView!
    @IBOutlet weak var imageCompleted: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewDate.layer.cornerRadius = 5
        self.viewTotal.layer.cornerRadius = 5
        self.viewMenu.layer.cornerRadius = 5
        self.viewDriver.layer.cornerRadius = 5
        self.imageCompleted.layer.cornerRadius = 30
        
    }
    @IBAction func backToHistory(_ sender: UIBarButtonItem) {
    
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
