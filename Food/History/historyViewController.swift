//
//  historyViewController.swift
//  Food
//
//  Created by GIS on 8/2/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class historyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var images = [#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2")]
    var locations = ["Street 598, Phnom Penh","Street 11, Phnom Penh","Street 22, Phnom Penh","Street 33, Phnom Penh","Street 44, Phnom Penh","Street 55, Phnom Penh"]
    var dates = ["2018/01/02","2018/02/02","2018/03/02","2018/04/02","2018/05/02","2018/06/02"]
    var times = ["4:30 AM","5:30 AM","6:30 AM","7:30 AM","8:30 AM"]
    
    @IBOutlet weak var tbVC: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbVC.delegate = self
        tbVC.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return times.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "History", for: indexPath) as! historyOrderTableViewCell
        
        cell.foodImage.image = images[indexPath.row]
        cell.orderLocation.text = locations[indexPath.row]
        cell.dateOrder.text = dates[indexPath.row]
        cell.timeOrder?.text = times[indexPath.row]
        
        //Change Style
        cell.viewHistory.layer.cornerRadius = 5
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let select = tableView.cellForRow(at: indexPath)
        self.performSegue(withIdentifier: "HistoryDetail", sender: select)
    }
    
    
    
}





