//
//  orderItemTableViewCell.swift
//  Food
//
//  Created by GIS on 7/27/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class orderItemTableViewCell: UITableViewCell {

    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var quality: UITextField!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var priceOfQuality: UILabel!
    @IBOutlet weak var minQuality: UIButton!
    @IBOutlet weak var addQuality: UIButton!
    @IBOutlet weak var Qualitys: UILabel!
    
}
