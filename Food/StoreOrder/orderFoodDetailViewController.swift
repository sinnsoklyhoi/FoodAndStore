//
//  orderFoodDetailViewController.swift
//  Food
//
//  Created by GIS on 8/2/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class orderFoodDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var viewDateAndTime: UIView!
    @IBOutlet weak var viewDetailPrice: UIView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var subTotalDiscount: UILabel!
    @IBOutlet weak var delivery: UILabel!
    @IBOutlet weak var Total: UILabel!
    @IBOutlet weak var viewOrder: UIView!
    @IBOutlet weak var buttonOrder: UIButton!
    @IBOutlet weak var orderLocation: UILabel!
    
    let images = ["image2", "image3"]
    
    public static var getLocation = ""
    
    @IBOutlet weak var tbCV: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        tbCV.dataSource = self
        tbCV.delegate = self
        
        //Change Style View
        self.viewDateAndTime.layer.cornerRadius = 5
        self.viewDetailPrice.layer.cornerRadius = 5
        self.viewOrder.layer.cornerRadius = 5
        
        // Current Date And Time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd"
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from:  date)
        let minute = calendar.component(.minute, from:  date)
        self.dateLabel.text = dateFormatter.string(from: date)
        self.timeLabel.text = String(hour) + ":" + String(minute) + " AM"
        print("date ", dateFormatter.string(from: date))
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.orderLocation.text = orderFoodDetailViewController.getLocation
        
        // Choose location
        if (orderLocation.text?.isEmpty)! {
            
            self.orderLocation.text = "Please choose location first"
            self.orderLocation.textColor = .red
            buttonOrder.isEnabled = false
        } else {
            
            self.orderLocation.textColor = .black
            buttonOrder.isEnabled = true
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderDetail", for: indexPath) as! itemFoodDetailTableViewCell
        
        cell.foodImage.image = UIImage(named: images[indexPath.row])
        cell.sizeFood.text = "small"
        cell.price.text = "$ 20.00"
        cell.quality.text = "1"
        
        //change style
        cell.viewFoodDetail.layer.cornerRadius = 5
        cell.sizeFood.layer.borderWidth = 1
        cell.sizeFood.layer.borderColor = UIColor.red.cgColor
        cell.sizeFood.layer.cornerRadius = 5
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let select = tableView.cellForRow(at: indexPath)
        self.performSegue(withIdentifier: "detailOrder", sender: select)
    }
    
    // Order Button
    @IBAction func orderButton(_ sender: UIButton) {

        var isLogin : Bool
        
        isLogin = UserDefaults.standard.bool(forKey: "isLogin")
        
        if isLogin {
            
            showAlertMessage(title: "ORDER", message: "Success!")
    
        } else {
            
            showAlertMessageLogin(title: "Sign In Your Account", message: "Your haven't sign in yet.")
        }
    
    }
    
    //Show Alert Message
    func showAlertMessage(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            //send Notification
//            NotificationCenter.default.post(name: Notification.Name("show"), object: nil)
            
            // Goto first page
            if let skipVC = self.navigationController?.viewControllers {
                
                for VC in skipVC {
                    
                    self.navigationController?.popToViewController(VC, animated: true)
                }
            }
        }
        
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertMessageLogin(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "YES", style: .default) { (action) in
            
            self.performSegue(withIdentifier: "orderFood", sender: nil)
        }
        
        let cencelAction = UIAlertAction(title: "NO", style: .default) { (action) in
        
        }
        
        alertController.addAction(cencelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertMessageLocation(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
        }
        
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func toMapButton(_ sender: UIButton) {
    
        performSegue(withIdentifier: "Order", sender: nil)
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "Order" {
            
            let desination = segue.destination as! ShowMapViewController
            desination.isSelectStore = false
        }
    }
    
    @IBAction func backToFoodDetail(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
}


















