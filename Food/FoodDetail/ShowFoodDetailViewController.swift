//
//  ShowFoodDetailViewController.swift
//  Food
//
//  Created by GIS on 7/25/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit

class ShowFoodDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var viewCheckout: UIView!
    @IBOutlet weak var qualityItem: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var checkOut: UIButton!
    
    @IBOutlet weak var tableVC: UITableView!
    
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var resOpen: UILabel!
    @IBOutlet weak var resClose: UILabel!
    @IBOutlet weak var viewResOpen: UIView!
    @IBOutlet weak var viewResClose: UIView!
    @IBOutlet weak var viewMenu1: UIView!
    @IBOutlet weak var viewMenu2: UIView!
    @IBOutlet weak var viewMenu3: UIView!
    
    
    var Food1s = ["A","B","C","D","E"]
    var images = [#imageLiteral(resourceName: "image1"),#imageLiteral(resourceName: "image2"),#imageLiteral(resourceName: "image3"),#imageLiteral(resourceName: "image4"),#imageLiteral(resourceName: "image5")]
    var prices = ["1$","2$","3$","4$","5$","6$"]
    var disPrices = ["0.5$","1.5$","2.5$","3.5$","4.5$"]
    var qualitys = [0,0,0,0,0]
    var total = [0,0,0,0,0]
    var storePrices = [String]()
    var labelQulity = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableVC.delegate = self
        tableVC.dataSource = self
        
        colorBorderView()
    
        // Give value from prices to storePrices
        storePrices = prices
        
        //cornerRadius On view
        self.viewResOpen.layer.cornerRadius = 5
        self.viewResOpen.layer.borderColor = UIColor.red.cgColor
        
        self.viewResClose.layer.cornerRadius = 5
        self.viewResClose.layer.borderColor = UIColor.red.cgColor
        
        self.viewMenu1.layer.cornerRadius = 5
        self.viewMenu1.layer.borderColor = UIColor.red.cgColor
        
        self.viewMenu2.layer.cornerRadius = 5
        self.viewMenu2.layer.borderColor = UIColor.red.cgColor
        
        self.viewMenu3.layer.cornerRadius = 5
        self.viewMenu3.layer.borderColor = UIColor.red.cgColor
        
        self.checkOut.layer.cornerRadius = 5
        
        //Call Fucntion
        createTabBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        qualitys = [0,0,0,0,0]
        storePrices = prices

        qualityItem.text = "0"
        labelQulity.text = "0"
        price.text = "0" + "$"
        tableVC.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Food1s.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Detail", for: indexPath) as! FoodDetailItemTableViewCell
        
        // strikethroughstyle on text
        let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: storePrices[indexPath.row])
        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedStringKey.strikethroughColor, value: UIColor.red, range: NSMakeRange(0, attributeString.length))
        
        cell.addQuality.layer.borderWidth = 1
        cell.addQuality.layer.borderColor = UIColor.gray.cgColor
        cell.addQuality.layer.cornerRadius = 15
        
        cell.minQuality.layer.borderWidth = 1
        cell.minQuality.layer.borderColor = UIColor.gray.cgColor
        cell.minQuality.layer.cornerRadius = 15
        
        cell.nameFood.text = Food1s[indexPath.row]
        cell.imageFood.image = images[indexPath.row]
        cell.disPrice.text = disPrices[indexPath.row]
        cell.Quality.text = String(qualitys[indexPath.row])
        cell.addQuality.tag = indexPath.row
        cell.minQuality.tag = indexPath.row
        cell.price.text = storePrices[indexPath.row]
        cell.price.attributedText = attributeString
    
        
        return cell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectCell = tableView.cellForRow(at: indexPath)
        performSegue(withIdentifier: "orderPage", sender: selectCell)
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        let convertQualityItemToInterger = (qualityItem.text! as NSString).integerValue
        
        if convertQualityItemToInterger == 0  {
            
            self.dismiss(animated: true, completion: nil)
            navigationController?.popViewController(animated: true)
            
        } else {
            
            showAlertMessage(title: "ORDER", message: "Do you want to close ? if you close your food will cleared")
        }
        
    }
    
    //Check Out Button
    @IBAction func checkOutButton(_ sender: UIButton) {
        
        let convertQualityItemToInterger = (qualityItem.text! as NSString).integerValue
        
        if convertQualityItemToInterger == 0 {
            
            showAlertMessageCheckOut(title: "ORDER", message: "Please order")
            
        } else {
            
            self.performSegue(withIdentifier: "orderFood", sender: nil)
        }
    }
    

    //Sub Quality Button
    @IBAction func subQualityButton(_ sender: UIButton) {
        
        if qualitys[sender.tag] == 0 {
            print("no")
            
        } else {
            qualitys[sender.tag] -= 1
            let totalQuality = qualitys.reduce(0, +)
            qualityItem.text = String(totalQuality)
            labelQulity.text = qualityItem.text
            
            let firstPrice = prices[sender.tag]
            storePrices.append(firstPrice)
            let convertPriceToString = (firstPrice as NSString).integerValue
            let totalPrice = String( convertPriceToString * qualitys[sender.tag])
            storePrices[sender.tag] = totalPrice
            
            //Add Total Price
            total[sender.tag] = (totalPrice as NSString).integerValue
            let totals = total.reduce(0, +)
            print("Total",totals)
            price.text = String(totals) + "$"
            print("pice",String(totals) + "$")
            tableVC.reloadData()
        }
    }
    
    @IBAction func addQualityButton(_ sender: UIButton) {
      
        qualitys[sender.tag] += 1
        let firstPrice = prices[sender.tag]
        storePrices.append(firstPrice)
        
        let convertPriceToString = (firstPrice as NSString).integerValue
        let totalPrice = String( convertPriceToString * qualitys[sender.tag])
        storePrices[sender.tag] = totalPrice
       
        //Add Quality Item
        let totalQuality = qualitys.reduce(0, +)
        qualityItem.text = String(totalQuality)
        labelQulity.text = qualityItem.text

        //Add Total Price
        total[sender.tag] = (totalPrice as NSString).integerValue
        let totals = total.reduce(0, +)
        print("Total",totals)
        price.text = String(totals) + "$"
        tableVC.reloadData()
        
        
    }
    //create TabBarButton
    func createTabBarButton()  {
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 40))
        button.setImage(UIImage(named: "myCard"), for: .normal)

        labelQulity.frame = CGRect(x: button.frame.width - 25, y: 0, width: 20, height: 20)
        labelQulity.text = "0"
        labelQulity.textAlignment = .center
        labelQulity.backgroundColor = .white
        labelQulity.clipsToBounds = true
        labelQulity.layer.borderWidth = 1
        labelQulity.layer.borderColor = UIColor.red.cgColor
        labelQulity.layer.cornerRadius = 10
        labelQulity.textColor = .black
        
        button.addSubview(labelQulity)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        button.addTarget(self, action: #selector(clickButton), for: .touchUpInside)
        
        
    }
    
    @objc func clickButton(){
        
        let convertLabelQualityToInterger = (labelQulity.text! as NSString).integerValue
        
        if convertLabelQualityToInterger == 0 {
            
        } else {
        
            self.performSegue(withIdentifier: "orderFood", sender: nil)
        }
    }
    
    func colorBorderView() {
        
        //Cycle picture
        viewImage.layer.cornerRadius = 40
        foodImage.layer.cornerRadius = 40
        // color border
        viewMenu1.layer.borderWidth = 1
        viewMenu1.layer.borderColor = UIColor.gray.cgColor
        viewMenu2.layer.borderWidth = 1
        viewMenu2.layer.borderColor = UIColor.gray.cgColor
        viewMenu3.layer.borderWidth = 1
        viewMenu3.layer.borderColor = UIColor.gray.cgColor
        viewResOpen.layer.borderWidth = 1
        viewResOpen.layer.borderColor = UIColor.gray.cgColor
        viewResClose.layer.borderWidth = 1
        viewResClose.layer.borderColor = UIColor.gray.cgColor
    }
    
    //Show Alert Message
    func showAlertMessage(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "NO", style: .default) { (action) in
            // Close current view controller
        }
        
        let cencelAction = UIAlertAction(title: "YES", style: .default) { (action) in
            // Close current view controller
            self.navigationController?.popViewController(animated: true)
            
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cencelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlertMessageCheckOut(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            // Close current view controller
        }
        
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
        
    }
}










